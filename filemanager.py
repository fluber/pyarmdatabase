import hashlib
from Crypto.Cipher import AES
import re
import ctypes
import ctypes.wintypes as wintypes

dll = ctypes.windll.LoadLibrary("./ET199_64.dll")

# ---------------------------------------------------------------------
# enum Return Value
(ET_S_SUCCESS,
 ET_E_KEY_REMOVED,
 ET_E_INVALID_PARAMETER,
 ET_E_COMM_ERROR,
 ET_E_INSUFFICIENT_BUFFER,
 ET_E_NO_LIST,
 ET_E_DEVPIN_NOT_CHECK,
 ET_E_USERPIN_NOT_CHECK,
 ET_E_RSA_FILE_FORMAT_ERROR,
 ET_E_DIR_NOT_FOUND,
 ET_E_ACCESS_DENIED,
 ET_E_ALREADY_INITIALIZED,
 ET_E_INCORRECT_PIN,
 ET_E_DF_SIZE,
 ET_E_FILE_EXIST,
 ET_E_UNSUPPORTED,
 ET_E_FILE_NOT_FOUND,
 ET_E_ALREADY_OPENED,
 ET_E_DIRECTORY_EXIST,
 ET_E_CODE_RANGE,
 ET_E_INVALID_POINTER,
 ET_E_GENERAL_FILESYSTEM,
 ET_E_OFFSET_BEYOND,
 ET_E_FILE_TYPE_MISMATCH,
 ET_E_PIN_BLOCKED,
 ET_E_INVALID_CONTEXT,
 ET_E_SHARING_VIOLATION,
 ET_E_ERROR_UNKNOWN,
 ET_E_LOAD_FILE_FAILED
 ) = map(int, [0,0xF0000001,0xF0000002,0xF0000003,0xF0000004,0xF0000005,
    0xF0000006,0xF0000007,0xF0000008,0xF0000009,0xF000000A,
    0xF000000B,0xF0000C00,0xF000000D,0xF000000E,0xF000000F,
    0xF0000010,0xF0000011,0xF0000012,0xF0000013,0xF0000014,
    0xF0000015,0xF0000016,0xF0000017,0xF0000018,0xF0000019,
    0xF000001A,0xFFFFFFFF,0xF0001001])

def ToString (ret_val):
    """ function to translate API return values into meaningful messages. """
    ErrorMap = {
        ET_S_SUCCESS                    : "Succeed." 
        ,ET_E_KEY_REMOVED               : "Device is not connected, or removed."
        , ET_E_INVALID_PARAMETER        : "Parameter is wrong."
        , ET_E_COMM_ERROR               : "Communication Error, example: timeout."
        , ET_E_INSUFFICIENT_BUFFER      : "Buffer is not enough."
        , ET_E_NO_LIST                  : "Not found device list."
        , ET_E_DEVPIN_NOT_CHECK         : "Developer password is not verified."
        , ET_E_USERPIN_NOT_CHECK        : "User password is not verified."
        , ET_E_RSA_FILE_FORMAT_ERROR    : "RSA Format Error."
        , ET_E_DIR_NOT_FOUND            : "Can not foud directory."
        , ET_E_ACCESS_DENIED            : "Access denied."
        , ET_E_ALREADY_INITIALIZED      : "The product has been initialized."
        , ET_E_INCORRECT_PIN            : "Incorrect password."
        , ET_E_DF_SIZE                  : "The specified directory space is not enough."
        , ET_E_FILE_EXIST               : "File already exists."
        , ET_E_UNSUPPORTED              : "The function is not supported or the file system has not been established."
        , ET_E_FILE_NOT_FOUND           : "The specified file was not found."
        , ET_E_ALREADY_OPENED           : "Card has been opened."
        , ET_E_DIRECTORY_EXIST          : "Directory already exists"
        , ET_E_CODE_RANGE               : "Virtual machine memory address overflow."
        , ET_E_INVALID_POINTER          : "Virtual machine error pointer."
        , ET_E_GENERAL_FILESYSTEM       : "General file system error."
        , ET_E_OFFSET_BEYOND            : "File offset exceeds file size."
        , ET_E_FILE_TYPE_MISMATCH       : "File type mismatch."
        , ET_E_PIN_BLOCKED              : "PIN code locked."
        , ET_E_INVALID_CONTEXT          : "ETContext parameter error."
        , ET_E_SHARING_VIOLATION        : "Another program is using this file and the process cannot access it."
        , ET_E_ERROR_UNKNOWN            : "Unknown error."
        , ET_E_LOAD_FILE_FAILED         : "Failed to download file."
    }
    if ret_val not in ErrorMap:
        raise ("UNKNOWN Error Return Value !!!")
    
    return ErrorMap[ret_val]                                 
class BaseStruct (ctypes.Structure):
    """ serves as a base class for all C-struct types """
    def as_dict(self):
        """ convert struct to dictionary """
        return dict([(field[0], getattr(self, field[0])) for field in self._fields_])
    def __str__(self):
        """ convert to string, via dictionary """
        return str(self.as_dict())
    def __getitem__(self, key):
        """ dictionary interface """
        return getattr(self, key)
    def __setitem__(self, key, value):
        """ dictionary interface """ 
        setattr(self, key, value)

class ET_CONTEXT(BaseStruct):
    _fields_ = [
            ('dwIndex', wintypes.DWORD),
            ('dwVersion', wintypes.DWORD),
            ('hLock', wintypes.HANDLE),
            ('reserve', wintypes.BYTE * 11),
            ('dwCustomer', wintypes.DWORD),
            ('bAtr', wintypes.BYTE * 16),
            ('bID', wintypes.BYTE * 8),
            ('dwAtrLen', wintypes.DWORD)]        

dll.ETEnum.argtypes = [ctypes.POINTER(ET_CONTEXT), ctypes.POINTER(wintypes.DWORD)]
dll.ETEnum.restype = wintypes.DWORD

def ETEnum():
    count = wintypes.DWORD(0)
    context = ET_CONTEXT()
    result = dll.ETEnum(ctypes.byref(context), ctypes.byref(count))
    return count.value

dll.ETOpen.argtypes = [ctypes.POINTER(ET_CONTEXT)]
dll.ETOpen.restype = wintypes.DWORD

def ETOpen(context):
    result = dll.ETOpen(ctypes.byref(context))
    assert result == ET_S_SUCCESS, ToString(result)
    return bytearray(context.bID).hex().upper()

dll.ETClose.argtypes = [ctypes.POINTER(ET_CONTEXT)]
dll.ETClose.restype = wintypes.DWORD

def ETClose(context):
    result = dll.ETClose(ctypes.byref(context))
    assert result == ET_S_SUCCESS, ToString(result)

class FileManager:
    def __init__(self, keyData):
        self.keyData = hashlib.sha256(bytes(keyData, "utf-8")).digest()
        self.ivData = hashlib.md5(bytes("鴻博資訊 見說文書將入境 今朝喜色上眉端", "utf-8")).digest()
        self.Address = None
        self.Authorized = None
        self.Year = None
        self.Month = None
        self.Error = True 
    
    def DecryptFile(self, licenseFile):
        try:
            with open(licenseFile, "rb") as f:
                cipheredData = f.read()
                cipher = AES.new(self.keyData, AES.MODE_CBC, iv=self.ivData)
                originalData = cipher.decrypt(cipheredData) 
                regex = re.compile("Address=\"(\\w+)\"\\sAuthorized=\"(\\w+)\"\\sYear=\"(\\w+)\"\\sMonth=\"(\\w+)\"\\sError=\"(\\w+)\"")
                match = regex.search(originalData.decode('utf-8'))
                self.Address = match.group(1)
                self.Authorized = bool(match.group(2))
                self.Year = int(match.group(3))
                self.Month = int(match.group(4))
                self.Error = bool(match.group(5))
        except Exception as ex:
            self.Address = None
            self.Authorized = None
            self.Year = None
            self.Month = None
            self.Error = True 
            