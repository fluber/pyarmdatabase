import os, glob

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

from CalibrationDailog import *
import logging
import logging.handlers
import json
import cv2
import numpy as np
from pypylon import pylon

class CalibrationDialog(QDialog, Ui_CalibrationDialog):

    def __init__(self, config):
        super(CalibrationDialog, self).__init__()
        self.setupUi(self)
        self.setWindowIcon(QIcon('arm.ico'))
        self.listWidget.setViewMode(QListView.IconMode)
        self.listWidget.setSpacing(10)
        self.listWidget.setIconSize(QSize(200, 200))
        self.listWidget.setMovement(False)
        self.listWidget.setResizeMode(QListView.Adjust)
        self.config = config
        self.image_dir = self.config['Calibration']['image_dir']
        self.edtImageDir.setText(self.image_dir)
        self.btnImageDir.clicked.connect(self.pick_image_dir)
        self.btnLoad.clicked.connect(self.load_images)
        self.btnReference.clicked.connect(self.set_reference)
        self.btnCalibration.clicked.connect(self.calibration)
        self.btnClear.clicked.connect(self.clear)
        self.btnExit.clicked.connect(self.close)
        self.btnSaveResult.clicked.connect(self.save_result)
        self.CameraMatrix = None
        self.DistCoeff = None
        self.RotationVector = None
        self.TranslationVector = None
        self.RotationMatrix = None
        devices = pylon.TlFactory.GetInstance().EnumerateDevices()
        for device in devices:
            self.cbCamera.addItem(device.GetSerialNumber())

    def pick_image_dir(self):
        self.btnSaveResult.setEnabled(False)
        if os.path.isdir(self.image_dir):
            self.image_dir = QFileDialog.getExistingDirectory(self.parent(), self.tr("Select directory with images"), self.config['Calibration']['image_dir'])
        else:
            self.image_dir = QFileDialog.getExistingDirectory(self.parent(), self.tr("Select directory with images"))

        if os.path.isdir(self.image_dir):
            self.config['Calibration']['image_dir'] = self.image_dir
            self.edtImageDir.setText(self.image_dir)

    def load_images(self):
        self.btnSaveResult.setEnabled(False)
        filenames = [f for f in glob.glob('{0}/*.bmp'.format(self.image_dir))]
        self.listWidget.clear()
        for filename in filenames:
            item = QListWidgetItem(QIcon(QPixmap(filename).scaled(200,200,  Qt.KeepAspectRatio, Qt.SmoothTransformation)), os.path.split(filename)[-1])
            self.listWidget.addItem(item)

    def clear(self):
        self.btnSaveResult.setEnabled(False)
        self.listWidget.clear()
        self.edtReference.setText('')

    def set_reference(self):
        self.btnSaveResult.setEnabled(False)
        if self.listWidget.selectedItems():
            item = self.listWidget.selectedItems().pop()
            if item:
                self.edtReference.setText(item.text())

    def calibration(self):
        if len(self.edtReference.text()) == 0:
            msg = QMessageBox()
            msg.setWindowIcon(QIcon('arm.ico'))
            msg.setIcon(QMessageBox.Critical)
            msg.setWindowTitle('Robot Arm Coordinate Monitor')
            msg.setText("Please ser reference at first.")
            msg.setStandardButtons(QMessageBox.Ok)
            msg.exec_()
            return
        
        try:
            self.CameraMatrix = None
            self.DistCoeff = None
            self.RotationVector = None
            self.TranslationVector = None
            self.RotationMatrix = None

            x,y = self.config['Calibration']['grid_shape'].split(',')
            # termination criteria
            criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
            # prepare object points, like (0,0,0), (1,0,0), (2,0,0).....(6,6,0)
            objp = np.zeros((int(x)*int(y),3), np.float32)
            objp[:,:2] = np.mgrid[0:7,0:7].T.reshape(-1,2) * float(self.config['Calibration']['center_distance'])
            # Arrays to store object points and image points from all the images.
            objpoints = [] # 3d point in read world space
            imgpoints = [] # 2d points in image plane

            images = glob.glob('{0}/*.bmp'.format(self.image_dir))

            total = len(images) + 1

            self.progressBar.setValue(0)
            iProgress = 0
            for fname in images:
                img = cv2.imread(fname)
                gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                ret, circles = cv2.findCirclesGrid(gray, (int(x),int(y)), flags = cv2.CALIB_CB_SYMMETRIC_GRID)
                if ret == True:
                    objpoints.append(objp)
                    imgpoints.append(circles)

                iProgress += 1
                self.progressBar.setValue(int((iProgress/total) * 100))

            ret, mtx, dist, rvec, tvec = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)

            # Arrays to store object points and image points from all the images.
            objpoints = [] # 3d point in read world space
            imgpoints = [] # 2d points in image plane
            fname = "{}/{}".format(self.image_dir, self.edtReference.text())
            img1 = cv2.imread(fname)
            img = img1.copy()
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            ret, circles = cv2.findCirclesGrid(gray, (int(x),int(y)), flags = cv2.CALIB_CB_SYMMETRIC_GRID)
            if ret == True:
                objpoints.append(objp)
                imgpoints.append(circles)

            iProgress += 1
            self.progressBar.setValue(int((iProgress/total) * 100))
            retval, rvec, tvec = cv2.solvePnP(objpoints[0], np.asarray(imgpoints[0].tolist()).reshape(int(x) * int(y),2), np.asarray(mtx), np.asarray(dist))
            rotMat,_ = cv2.Rodrigues(rvec)

            self.CameraMatrix = mtx 
            self.DistCoeff = dist 
            self.RotationVector = rvec 
            self.TranslationVector = tvec 
            self.RotationMatrix = rotMat 

            self.txtCameraMatrix.setText(str(self.CameraMatrix))
            self.txtDistCoeff.setText(str(self.DistCoeff))
            self.txtRotationVector.setText(str(self.RotationVector))
            self.txtTranslationVector.setText(str(self.TranslationVector))
            self.txtRotationMatrix.setText(str(self.RotationMatrix))

            self.btnSaveResult.setEnabled(True)

        except:
            logging.error("Catch an exception.", exc_info=True)

    def save_result(self):
        try:
            sn = self.cbCamera.currentText()
            dirname = "{}".format(os.path.dirname(os.path.abspath(__file__)))
            data = {"camera_matrix": self.CameraMatrix.tolist(), "dist_coeff": self.DistCoeff.tolist()}
            fname = "{}/{}_camera_calibration.json".format(dirname, sn)
            import json
            with open(fname, "w") as f:
                json.dump(data, f)

            data = {"rotation_vector": self.RotationVector.tolist(), "translation_vector": self.TranslationVector.tolist(), "rotation_matrix": self.RotationMatrix.tolist()}
            fname = "{}/{}_pose.json".format(dirname, sn)
            import json
            with open(fname, "w") as f:
                json.dump(data, f)

            msg = QMessageBox()
            msg.setWindowIcon(QIcon('arm.ico'))
            msg.setIcon(QMessageBox.Information)
            msg.setWindowTitle('Robot Arm Coordinate Monitor')
            msg.setText("Save Successfully.")
            msg.setStandardButtons(QMessageBox.Ok)
            msg.exec_()
            self.progressBar.setValue(0)
        except:
            logging.error("Catch an exception.", exc_info=True)
            self.progressBar.setValue(0)