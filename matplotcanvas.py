import matplotlib as mpl
mpl.use("Qt5Agg")
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar

import numpy as np
from threading import Timer 

import logging
import logging.handlers

class Qt5MplCanvas(FigureCanvas):
    def __init__(self, arm, xy, xlim, ylim, bandInfo, parent=None):
        self.arm = arm
        self.xy = xy
        self.fig = Figure()
        self.axes = self.fig.add_subplot(111)
        self.xlim = xlim
        self.ylim = ylim
        self.axes.set_xlim(self.xlim)
        self.axes.set_ylim(self.ylim)
        self.axes.set_title("Arm 1" if self.arm == 0 else "Arm 2")
        if self.xy == 0:
            self.axes.set_ylabel('X Coordinate')
        else:
            self.axes.set_ylabel('Y Coordinate')
        self.axes.set_xlabel('Records')
        super(Qt5MplCanvas, self).__init__(figure=self.fig)
        self.setParent(parent)
        for info in bandInfo:
            if info[0] == self.arm and info[1]== self.xy:
                 self.axes.axhspan(ymin=info[3], ymax=info[4], alpha=0.3, color=info[2])

    def update_data(self, data, bandInfo):
        try:
            x = range(len(data))
            self.axes.clear()
            self.axes.set_ylim(self.ylim)
            self.axes.set_xlim(self.xlim)
            self.axes.set_title("Arm 1" if self.arm == 0 else "Arm 2")
            if self.xy == 0:
                self.axes.set_ylabel('X Coordinate')
            else:
                self.axes.set_ylabel('Y Coordinate')
            self.axes.set_xlabel('Records')
            for info in bandInfo:
                if info[0] == self.arm and info[1]== self.xy:
                     self.axes.axhspan(ymin=info[3], ymax=info[4], alpha=0.3, color=info[2])
            self.axes.scatter(x = x, y = data, c = 'r', alpha=0.8, marker = '.', label='X Coordinate')  
            self.draw()
        except:
            logging.error("Catch an exception.", exc_info=True)



class RepeatingTimer(Timer): 
    def run(self):
        while not self.finished.is_set():
            self.function(*self.args, **self.kwargs)
            self.finished.wait(self.interval)