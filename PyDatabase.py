import os, sys

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

from DatabaseWindow import *
from filemanager import *
import psycopg2
import logging
import logging.handlers
import configparser
import time
import schedule
import threading
from datetime import datetime

class PyDatabaseWindow(QMainWindow, Ui_DatabaseWindow):

    def __init__(self, parent=None, application=None):
        super(PyDatabaseWindow, self).__init__(parent)
        self.setupUi(self)
        self.config = configparser.ConfigParser()
        self.config.read('PyDatabase.ini')
        self.setWindowIcon(QIcon('database.ico'))
        self.edtOutput.setText(self.config['Database']['output'])
        self.btnDatabase.clicked.connect(self.runDatabaseTool)
        self.btnLog.clicked.connect(self.openLog)
        self.btnOutput.clicked.connect(self.pick_output)
        self.btnStart.clicked.connect(self.start)
       	self.btnExit.clicked.connect(self.close)
        self.btnClear.clicked.connect(self.clear)
        self.initSchedule()
	
    def showEvent(self, event):
        super().showEvent(event)

    def closeEvent(self,event):
        self.save_ini()
        result = QMessageBox.question(self,
                      "Confirm Exit...",
                      "Are you sure you want to exit ?",
                      QMessageBox.Yes| QMessageBox.No)
        event.ignore()

        if result == QMessageBox.Yes:
            if self.btnStart.text() == "Stop":
                self.start()

            event.accept()

    def initSchedule(self):
        (kind, value) = self.config['Database']['schedule'].split(',')
        if kind == 'minute':
            self.rbMinute.setChecked(True)
            self.boxMinute.setValue(int(value))
        elif kind == 'hour':
            self.rbHour.setChecked(True)
            self.boxHour.setValue(int(value))
        elif kind == 'day':
            self.rbDay.setChecked(True)
            self.boxDay.setValue(int(value))
        elif kind == 'week':
            self.rbWeek.setChecked(True)
            self.boxWeek.setValue(int(value))

            

    def openLog(self):
        log_dir = "{}\\log".format(os.path.dirname(os.path.abspath(__file__)))
        os.startfile(log_dir)
    
    def runDatabaseTool(self):
        os.popen(self.config['Database']['tool'])

    def pick_output(self):
        folderpath = QtWidgets.QFileDialog.getExistingDirectory(self, 'Select Folder')
        self.edtOutput.setText(folderpath)

    def task(self):
        try:
            now = datetime.now()
            sn = now.strftime("%Y%m%d%H%M%S")
            if os.path.isdir(self.edtOutput.text()):
                dir_path = "{}/".format(self.edtOutput.text())

                cmd = "DROP TABLE IF EXISTS robot_arm_coordinate_{0};CREATE TABLE robot_arm_coordinate_{0} AS TABLE robot_arm_coordinate; DELETE FROM robot_arm_coordinate WHERE arm >= 0; ".format(sn)
                self.lstMessage.addItem("{} : {}".format(now.strftime("%Y/%m/%d-%H:%M:%S"), cmd))
                cur = self.db.cursor()
                cur.execute(cmd)

                csv_cmd = "copy robot_arm_coordinate_{0} to STDOUT csv header;".format(sn)
                self.lstMessage.addItem("{} : {}".format(now.strftime("%Y/%m/%d-%H:%M:%S"), csv_cmd))
                self.lstMessage.addItem("backup to {1}robot_arm_coordinate_{0}.csv".format(sn, dir_path))
                with open("{1}robot_arm_coordinate_{0}.csv".format(sn, dir_path),'w') as f:
                    cur.copy_expert(csv_cmd, f)
                self.db.commit()
            else:
                cmd = "{} is not directory.".format(self.edtOutput.text())
                self.lstMessage.addItem("{} : {}".format(now.strftime("%Y/%m/%d-%H:%M:%S"), cmd))
        except:
            self.db.rollback()
            logging.error("Catch an exception.", exc_info=True)

    def clear(self):
        self.lstMessage.clear()

    def loop(self):
        while self.do_loop:
            schedule.run_pending()
            time.sleep(1)

    def start(self):
        if self.btnStart.text() == "Start":
            if self.rbMinute.isChecked():
                self.job = schedule.every(self.boxMinute.value()).minutes.do(self.task)
            elif self.rbHour.isChecked():
                self.job = schedule.every(self.boxHour.value()).hours.do(self.task)
            elif self.rbDay.isChecked():
                self.job = schedule.every(self.boxDay.value()).days.do(self.task)
            elif self.rbWeek.isChecked():
                self.job = schedule.every(self.boxWeek.value()).weeks.do(self.task)
            else:
                self.job = None
            self.btnStart.setText("Stop")
            self.btnStart.setStyleSheet("background-color: rgb(170, 255, 127);")
            self.do_loop = True
            self.schedule_thread = threading.Thread(target = self.loop)
            self.schedule_thread.start()

        else:
            if self.job == None:
               schedule.cancel_job(self.job) 
               self.job = None
            self.do_loop = False 
            self.schedule_thread.join()
            self.btnStart.setText("Start")
            self.btnStart.setStyleSheet("background-color: rgb(255, 170, 255);")


    def save_ini(self):
        if self.rbMinute.isChecked():
            self.config['Database']['schedule'] = "minute,{}".format(self.boxMinute.value())
        elif self.rbHour.isChecked():
            self.config['Database']['schedule'] = "hour,{}".format(self.boxHour.value())
        elif self.rbDay.isChecked():
            self.config['Database']['schedule'] = "day,{}".format(self.boxDay.value())
        elif self.rbWeek.isChecked():
            self.config['Database']['schedule'] = "week,{}".format(self.boxWeek.value())

        self.config['Database']['output'] = self.edtOutput.text()
        with open('PyDatabase.ini','w') as configfile:
            self.config.write(configfile)



    def initDatabase(self):
        try:
            self.db = psycopg2.connect(database=self.config['Database']['DBname'], user=self.config['Database']['User'], password=self.config['Database']['Password'], host=self.config['Database']['Host'], port=self.config['Database']['Port'])
            return (True, 'OK')
        except Exception as ex:
            return (False, str(ex))


if __name__ == '__main__':
    application = QApplication(sys.argv)

    log_dir = "{}/log".format(os.path.dirname(os.path.abspath(__file__)))
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)
    log_file = "{}/databae_log.txt".format(log_dir)

    formatter = logging.Formatter('%(asctime)s - %(filename)s:%(lineno)d - %(levelname)s - %(message)s')
    formatter.converter = time.gmtime

    fileHandler = logging.handlers.RotatingFileHandler(log_file, maxBytes=1024 * 1024 * 5, backupCount=10)
    fileHandler.setFormatter(formatter)

    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(formatter)

    logger = logging.getLogger()
    logger.addHandler(fileHandler)
    logger.addHandler(consoleHandler)
    

    if len(sys.argv) > 1:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.ERROR)


    license_file = 'license.dat'
    has_dongle = ETEnum() > 0
    if has_dongle and os.path.isfile(license_file):
        splash = QSplashScreen(QPixmap('logo.jpg'))
        splash.show()

        context = ET_CONTEXT()
        dongle_sn = ETOpen(context)
        ETClose(context)
        
        fileManager = FileManager("望渠消息向長安 常把菱花仔細看")
        fileManager.DecryptFile(license_file)
        if fileManager.Address == dongle_sn:
            window = PyDatabaseWindow()
            db_ok = window.initDatabase()
            if db_ok[0]:
                splash.close()
                window.show()
                sys.exit(application.exec_())
            else:
                splash.close()
                msg = QMessageBox()
                msg.setWindowIcon(QIcon('database.ico'))
                msg.setIcon(QMessageBox.Critical)
                msg.setWindowTitle('Robot Arm Coordinate Monitor')
                if db_ok[0] == False:
                    msg.setText("{} The Application will shutdown.".format(db_ok[1]))
                logging.error(msg.text())
                msg.setStandardButtons(QMessageBox.Ok)
                msg.exec_()
        else:
            logging.error("The dongle is not match with licene file. The Application will shutdown.")
            splash.close()
            msg = QMessageBox()
            msg.setWindowIcon(QIcon('database.ico'))
            msg.setIcon(QMessageBox.Critical)
            msg.setWindowTitle('Robot Arm Coordinate Databae')
            msg.setText("The dongle is not match with licene file. The Application will shutdown.")
            msg.setStandardButtons(QMessageBox.Ok)
            msg.exec_()

    else:
        logging.error("Don't find license file or dongle. The Application will shutdown.")
        msg = QMessageBox()
        msg.setWindowIcon(QIcon('database.ico'))
        msg.setIcon(QMessageBox.Critical)
        msg.setWindowTitle('Robot Arm Coordinate Database')
        msg.setText("Don't find license file or dongle. The Application will shutdown.")
        msg.setStandardButtons(QMessageBox.Ok)
        msg.exec_()