from BandDialog import *
from NewBandDialog import *

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

from matplotcanvas import *
from matplotlib.widgets import SpanSelector

import psycopg2
import logging
import logging.handlers

class BandDialog(QDialog, Ui_BandDialog):

    def __init__(self, arm, xy, db, xlim, ylim, bandInfo):
        super(BandDialog, self).__init__()
        self.setupUi(self)
        self.setWindowIcon(QIcon('arm.ico'))
        self.arm = arm
        self.xy = xy
        self.db = db
        self.xlim = xlim
        self.ylim = ylim
        self.bandInfo = bandInfo

        self.btnNew.clicked.connect(self.new_band)
        self.btnDelete.clicked.connect(self.delete_band)
        self.btnClose.clicked.connect(self.close)

        self.model = QStandardItemModel()
        self.model.setColumnCount(4)
        self.model.setHeaderData(0, Qt.Horizontal, 'Color')
        self.model.setHeaderData(1, Qt.Horizontal, 'Min')
        self.model.setHeaderData(2, Qt.Horizontal, 'Max')
        self.model.setHeaderData(3, Qt.Horizontal, 'Alarm')

        self.initChart()

        self.tableView.setModel(self.model)
        self.select()

    def onselect(self, ymin, ymax):
        print(ymin, ymax)
        try:
            dialog = NewBandDialog(self.arm, self.xy, self.db, ymin, ymax)
            dialog.exec_()
            self.select()
        except:
            logging.error("Catch an exception.", exc_info=True)

    def initChart(self):
        vbl = QVBoxLayout(self.widget)
        self.chart = Qt5MplCanvas(self.arm, self.xy, self.xlim, self.ylim, self.bandInfo, self.widget)
        self.span = SpanSelector(self.chart.axes, self.onselect, 'vertical', useblit=True,
                    rectprops=dict(alpha=0.5, facecolor='red'))
        vbl.addWidget(self.chart)

    def select(self):
        try:
            cur = self.db.cursor()
            cur.execute("SELECT color, min, max, alarm from band where arm = {} and xy = {} order by max desc".format(self.arm, self.xy))
            rows = cur.fetchall()
            i = 0
            self.chart.axes.clear()
            self.chart.axes.set_xlim(self.xlim)
            self.chart.axes.set_ylim(self.ylim)
            for row in rows:
                self.model.setItem(i, 0, QStandardItem(row[0]))
                self.model.setItem(i, 1, QStandardItem(str(row[1])))
                self.model.setItem(i, 2, QStandardItem(str(row[2])))
                self.chart.axes.axhspan(ymin=row[1], ymax=row[2], alpha=0.5, color=row[0])
                self.model.setItem(i, 3, QStandardItem('True' if row[3] == 2 else 'False'))
                self.model.item(i,0).setBackground(QBrush(QColor(row[0]))); 
                self.model.item(i,1).setBackground(QBrush(QColor(row[0]))); 
                self.model.item(i,2).setBackground(QBrush(QColor(row[0]))); 
                self.model.item(i,3).setBackground(QBrush(QColor(row[0]))); 
                # self.model.setItem(i, 2, QSpacerItem(float(row[2])))
                # self.model.setItem(i, 3, QSpacerItem(row[3]))
                i = i + 1
            self.chart.draw()
        except:
            logging.error("Catch an exception.", exc_info=True)
    
    def new_band(self):
        try:
            dialog = NewBandDialog(self.arm, self.xy, self.db)
            dialog.exec_()
            self.select()
        except:
            logging.error("Catch an exception.", exc_info=True)

    def delete_band(self):
        try:
            if self.tableView.currentIndex():
                row = self.model.itemFromIndex(self.tableView.currentIndex()).row()
                color = self.model.takeRow(row)[0].text()
                cur = self.db.cursor()
                cur.execute("DELETE FROM band WHERE arm = {} and xy = {} and color = \'{}\' ".format(self.arm, self.xy, color))
                self.db.commit()
                self.select()
        except:
            logging.error("Catch an exception.", exc_info=True)

        

class NewBandDialog(QDialog, Ui_NewBandDialog):

    def __init__(self, arm, xy, db, min = None, max = None):
        super(NewBandDialog, self).__init__()
        self.setupUi(self)
        self.setWindowIcon(QIcon('arm.ico'))
        self.arm = arm
        self.xy = xy
        self.db = db
        self.btnCancel.clicked.connect(self.close)
        self.btnColor.clicked.connect(self.pick_color)
        self.btnOK.clicked.connect(self.new_band)
        if min:
            self.boxMin.setValue(min)

        if max:
            self.boxMax.setValue(max)

    def pick_color(self):
        color = QColorDialog.getColor()
        if color:
            self.edtColor.setText(color.name())
            self.edtColor.setStyleSheet("background-color: {};".format(color.name()))

    def new_band(self):
        try:
            cur = self.db.cursor()
            cur.execute("INSERT INTO band(arm, xy, color, min, max, alarm) VALUES({}, {}, \'{}\', {}, {}, {})".format(self.arm, self.xy, self.edtColor.text(), self.boxMin.value(), self.boxMax.value(), 2 if self.cbAlarm.isChecked() else 0))
            self.db.commit()
            self.close()
        except:
            logging.error("Catch an exception.", exc_info=True)

        


