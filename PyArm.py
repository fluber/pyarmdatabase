import os, sys

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

from pypylon import pylon
import cv2
import numpy as np
import json
import time
from MainWindow import *
from GraphicsRectItem import *
from filemanager import *
from matplotcanvas import *
from band import *
from PyCalibration import *

import psycopg2
import logging
import logging.handlers
import configparser

class SampleImageEventHandler(QObject, pylon.ImageEventHandler):
    updateImage = QtCore.pyqtSignal(np.ndarray)
    def __init__(self):
        super(QObject, self).__init__()
        super(pylon.ImageEventHandler, self).__init__()
        self.converter = pylon.ImageFormatConverter()
        self.converter.OutputPixelFormat = pylon.PixelType_RGB8packed
        self.converter.OutputBitAlignment = pylon.OutputBitAlignment_MsbAligned


    def OnImageGrabbed(self, camera, grabResult):
        try:
            if grabResult and grabResult.GrabSucceeded():
                image = self.converter.Convert(grabResult)
                raw_data = image.GetArray()
                self.updateImage.emit(raw_data)
        except:
            logging.error("Catch an exception.", exc_info=True)


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None, application=None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)
        self.config = configparser.ConfigParser()
        self.config.read('PyArm.ini')
        self.setWindowIcon(QIcon('arm.ico'))
        self.m_pixmap1 = QGraphicsPixmapItem()
        self.m_roi1 = GraphicsRectItem(0, 0, int(self.config['Arm1']['ROI_W']), int(self.config['Arm1']['ROI_H']))
        self.m_roi1.setParentItem(self.m_pixmap1)
        self.m_roi1.setPen(QPen(QColor('red')))
        self.m_roi1.setFlag(QGraphicsItem.ItemIsMovable)
        self.graphicsView1.setScene(QGraphicsScene(self))
        self.graphicsView1.scene().addItem(self.m_pixmap1)
        self.graphicsView1.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.graphicsView1.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.m_roi1.moveBy(int(self.config['Arm1']['ROI_X']), int(self.config['Arm1']['ROI_Y']))
        # self.graphicsView1.scene().addItem(self.m_roi1)
        self.m_pixmap2 = QGraphicsPixmapItem()
        self.m_roi2 = GraphicsRectItem(0, 0, int(self.config['Arm2']['ROI_W']), int(self.config['Arm2']['ROI_H']))
        self.m_roi2.setParentItem(self.m_pixmap2)
        self.m_roi2.setPen(QPen(QColor('red')))
        self.m_roi2.setFlag(QGraphicsItem.ItemIsMovable)
        self.graphicsView2.setScene(QGraphicsScene(self))
        self.graphicsView2.scene().addItem(self.m_pixmap2)
        self.graphicsView2.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.graphicsView2.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.m_roi2.moveBy(int(self.config['Arm2']['ROI_X']), int(self.config['Arm2']['ROI_Y']))
        # self.graphicsView2.scene().addItem(self.m_roi2)
        self.actionCalibration.triggered.connect(self.show_calibration)
        self.actionExit.triggered.connect(self.close)
        self.btnClear.clicked.connect(self.clearAlarm)
        self.btnLog.clicked.connect(self.openLog)
        self.btnDatabase.clicked.connect(self.runDatabaseTool)
        self.btnExit.clicked.connect(self.close)
        self.btnOpen1.clicked.connect(self.open_close_camera1)
        self.btnOpen2.clicked.connect(self.open_close_camera2)
        self.btnStart1.clicked.connect(self.start_stop_camera1)
        self.btnStart2.clicked.connect(self.start_stop_camera2)
        self.btnStart1.setEnabled(False)
        self.btnStart2.setEnabled(False)
        self.slExposure1.valueChanged.connect(self.change_exposure1)
        self.slExposure2.valueChanged.connect(self.change_exposure2)
        self.slExposure1.setEnabled(False)
        self.slExposure2.setEnabled(False)
        self.slGain1.valueChanged.connect(self.change_gain1)
        self.slGain2.valueChanged.connect(self.change_gain2)
        self.slGain1.setEnabled(False)
        self.slGain2.setEnabled(False)
        self.slThreshold1.valueChanged.connect(self.change_threshold1)
        self.slThreshold2.valueChanged.connect(self.change_threshold2)
        self.slThreshold1.setValue(int(self.config['Arm1']['threshold']))
        self.slThreshold2.setValue(int(self.config['Arm2']['threshold']))
        self.cbDark1.setCheckState(int(self.config['Arm1']['dark']))
        self.cbDark2.setCheckState(int(self.config['Arm2']['dark']))
        self.slThreshold1.setEnabled(False)
        self.slThreshold2.setEnabled(False)
        self.cbDark1.stateChanged.connect(self.change_dark1)
        self.cbDark2.stateChanged.connect(self.change_dark2)
        self.cbDark1.setEnabled(False)
        self.cbDark2.setEnabled(False)
        self.cbFit1.setCheckState(int(self.config['Arm1']['fit']))
        self.cbFit2.setCheckState(int(self.config['Arm2']['fit']))
        self.cbFit1.setEnabled(False)
        self.cbFit2.setEnabled(False)
        self.boxArea1.setValue(int(self.config['Arm1']['area']))
        self.boxArea2.setValue(int(self.config['Arm2']['area']))
        self.boxArea1.setEnabled(False)
        self.boxArea2.setEnabled(False)
        self.boxIgnore1.setValue(int(self.config['Arm1']['ignore']))
        self.boxIgnore2.setValue(int(self.config['Arm2']['ignore']))
        self.boxIgnore1.setEnabled(False)
        self.boxIgnore2.setEnabled(False)
        self.ignoreCount1 = 0
        self.ignoreCount2 = 0
        self.camera1 = None
        self.camera2 = None
        self.converter1 = pylon.ImageFormatConverter()
        self.converter1.OutputPixelFormat = pylon.PixelType_RGB8packed
        self.converter1.OutputBitAlignment = pylon.OutputBitAlignment_MsbAligned
        self.converter2 = pylon.ImageFormatConverter()
        self.converter2.OutputPixelFormat = pylon.PixelType_RGB8packed
        self.converter2.OutputBitAlignment = pylon.OutputBitAlignment_MsbAligned
        self.stop_t1 = True 
        self.stop_t2 = True 

        self.arm1_x_xlim = [int(self.config['Arm1']['x_xlim'].split(',')[0]), int(self.config['Arm1']['x_xlim'].split(',')[1])]
        self.arm1_x_ylim = [int(self.config['Arm1']['x_ylim'].split(',')[0]), int(self.config['Arm1']['x_ylim'].split(',')[1])]
        self.arm1_y_xlim = [int(self.config['Arm1']['y_xlim'].split(',')[0]), int(self.config['Arm1']['y_xlim'].split(',')[1])]
        self.arm1_y_ylim = [int(self.config['Arm1']['y_ylim'].split(',')[0]), int(self.config['Arm1']['y_ylim'].split(',')[1])]

        self.arm2_x_xlim = [int(self.config['Arm2']['x_xlim'].split(',')[0]), int(self.config['Arm2']['x_xlim'].split(',')[1])]
        self.arm2_x_ylim = [int(self.config['Arm2']['x_ylim'].split(',')[0]), int(self.config['Arm2']['x_ylim'].split(',')[1])]
        self.arm2_y_xlim = [int(self.config['Arm2']['y_xlim'].split(',')[0]), int(self.config['Arm2']['y_xlim'].split(',')[1])]
        self.arm2_y_ylim = [int(self.config['Arm2']['y_ylim'].split(',')[0]), int(self.config['Arm2']['y_ylim'].split(',')[1])]

        self.arm1_x_chart_data = []
        self.arm1_y_chart_data = []
        self.arm2_x_chart_data = []
        self.arm2_y_chart_data = []

    def showEvent(self, event):
        super().showEvent(event)
        self.arm1_x_chart = self.initChart(0, 0, self.arm1_x_xlim, self.arm1_x_ylim, self.bandInfo, self.wgArm1X, self.arm1_x_config)
        self.arm1_y_chart = self.initChart(0, 1, self.arm1_y_xlim, self.arm1_y_ylim, self.bandInfo, self.wgArm1Y, self.arm1_y_config)
        self.arm2_x_chart = self.initChart(1, 0, self.arm2_x_xlim, self.arm2_x_ylim, self.bandInfo, self.wgArm2X, self.arm2_x_config)
        self.arm2_y_chart = self.initChart(1, 1, self.arm2_y_xlim, self.arm2_y_ylim, self.bandInfo, self.wgArm2Y, self.arm2_y_config)

    def closeEvent(self,event):
        self.save_ini()
        result = QMessageBox.question(self,
                      "Confirm Exit...",
                      "Are you sure you want to exit ?",
                      QMessageBox.Yes| QMessageBox.No)
        event.ignore()

        if result == QMessageBox.Yes:
            if self.btnStart1.text() == "Stop":
                self.start_stop_camera1()

            if self.btnStart2.text() == "Stop":
                self.start_stop_camera2()

            event.accept()

    def loadCalibration(self, sn):
        fname = "{}_camera_calibration.json".format(sn)
        with open(fname) as f:
            data = json.load(f)
        camera_matrix = np.asarray(data['camera_matrix'])
        dist_coeff = np.asarray(data['dist_coeff'])

        fname = "{}_pose.json".format(sn)
        with open(fname) as f:
            data = json.load(f)
        rotation_vector = np.asarray(data['rotation_vector'])
        translation_vector = np.asarray(data['translation_vector'])
        rotation_matrix = np.asarray(data['rotation_matrix'])
        return (camera_matrix, dist_coeff, rotation_vector, translation_vector, rotation_matrix)

    def insertAlarm(self, arm, xy, x, y):
        if self.ltAlarm.count() > 20:
            self.ltAlarm.clear()
        
        self.wgAlarm.setStyleSheet("background-color: rgb(255, 0, 0);")

        if arm == 0 and xy == 0:    
            self.wgArm1X.setStyleSheet("background-color: rgb(255, 0, 0);")
        elif arm == 0 and xy == 1:    
            self.wgArm1Y.setStyleSheet("background-color: rgb(255, 0, 0);")
        elif arm == 1 and xy == 0:    
            self.wgArm2X.setStyleSheet("background-color: rgb(255, 0, 0);")
        else: 
            self.wgArm2Y.setStyleSheet("background-color: rgb(255, 0, 0);")

        if xy == 0:
            self.ltAlarm.addItem("{0} {1}: {2:.2f}".format("Arm1" if arm == 0 else "Arm2", "X", x))
        else:
            self.ltAlarm.addItem("{0} {1}: {2:.2f}".format("Arm1" if arm == 0 else "Arm2", "Y", y))


    def clearAlarm(self):
        self.wgAlarm.setStyleSheet("")
        self.wgArm1X.setStyleSheet("")
        self.wgArm1Y.setStyleSheet("")
        self.wgArm2X.setStyleSheet("")
        self.wgArm2Y.setStyleSheet("")
        self.ltAlarm.clear()

    def openLog(self):
        log_dir = "{}\\log".format(os.path.dirname(os.path.abspath(__file__)))
        os.startfile(log_dir)
    
    def runDatabaseTool(self):
        os.popen(self.config['Database']['Tool'])

    def save_ini(self):
        self.config['Arm1']['Camera'] = self.cbCamera1.currentText() 
        self.config['Arm2']['Camera'] = self.cbCamera2.currentText() 

        roi_x =  int(self.m_roi1.boundingRect().x() + 4 + self.m_roi1.pos().x())
        roi_y =  int(self.m_roi1.boundingRect().y() + 4 + self.m_roi1.pos().y())
        roi_w = int(self.m_roi1.boundingRect().width() - 8)
        roi_h = int(self.m_roi1.boundingRect().height() - 8)

        self.config['Arm1']['roi_x'] = str(roi_x)
        self.config['Arm1']['roi_y'] = str(roi_y)
        self.config['Arm1']['roi_w'] = str(roi_w)
        self.config['Arm1']['roi_h'] = str(roi_h)
        
        self.config['Arm1']['threshold'] = str(self.slThreshold1.value())
        self.config['Arm1']['dark'] = '2' if self.cbDark1.isChecked() else '0' 
        self.config['Arm1']['fit'] = '2' if self.cbFit1.isChecked() else '0' 
        self.config['Arm1']['ignore'] = str(self.boxIgnore1.value()) 
        self.config['Arm1']['area'] = str(self.boxArea1.value()) 

        roi_x =  int(self.m_roi2.boundingRect().x() + 4 + self.m_roi2.pos().x())
        roi_y =  int(self.m_roi2.boundingRect().y() + 4 + self.m_roi2.pos().y())
        roi_w = int(self.m_roi2.boundingRect().width() - 8)
        roi_h = int(self.m_roi2.boundingRect().height() - 8)

        self.config['Arm2']['roi_x'] = str(roi_x)
        self.config['Arm2']['roi_y'] = str(roi_y)
        self.config['Arm2']['roi_w'] = str(roi_w)
        self.config['Arm2']['roi_h'] = str(roi_h)

        self.config['Arm2']['threshold'] = str(self.slThreshold2.value())
        self.config['Arm2']['dark'] = '2' if self.cbDark2.isChecked() else '0' 
        self.config['Arm2']['fit'] = '2' if self.cbFit2.isChecked() else '0' 
        self.config['Arm2']['ignore'] = str(self.boxIgnore1.value()) 
        self.config['Arm2']['area'] = str(self.boxArea2.value()) 


        with open('PyArm.ini','w') as configfile:
            self.config.write(configfile)



    def initDatabase(self):
        try:
            self.db = psycopg2.connect(database=self.config['Database']['DBname'], user=self.config['Database']['User'], password=self.config['Database']['Password'], host=self.config['Database']['Host'], port=self.config['Database']['Port'])
            self.queryBandInfo()
            return (True, 'OK')
        except Exception as ex:
            return (False, str(ex))

    def queryBandInfo(self):
        try:
            cur = self.db.cursor()
            cur.execute("SELECT arm, xy, color, min, max, alarm from band order by arm, xy, max desc")
            rows = cur.fetchall()
            self.bandInfo = [] 
            for row in rows:
                self.bandInfo.append([row[0], row[1], row[2], row[3], row[4], row[5]])
        except:
            logging.error("Catch an exception.", exc_info=True)


    def checkAlarm(self, arm, x, y):
        alarmX = False
        alarmY = False

        # Check coordinate in the alarm range
        for info in self.bandInfo:
            if info[0] == arm and info[1] == 0 and info[5] > 0:
                # X
                if x >= info[3] and x <= info[4]:
                    alarmX = True
            elif info[0] == arm and info[1] == 1 and info[5] > 0:
                # Y 
                if y >= info[3] and y <= info[4]:
                   alarmY = True 
        # Check coordinate in ths safe range
        if alarmX:
            for info in self.bandInfo:
                if info[0] == arm and info[1] == 0 and info[5] == 0:
                    if x >= info[3] and x <= info[4]:
                        alarmX = False 
                        break
        if alarmY:
            for info in self.bandInfo:
                if info[0] == arm and info[1] == 1 and info[5] == 0:
                    if y >= info[3] and y <= info[4]:
                        alarmY = False 
                        break

        return (alarmX, alarmY)



    def insertTable(self, arm, x, y, alarmX, alarmY):
        try:
            cur = self.db.cursor()
            cur.execute("INSERT INTO robot_arm_coordinate(arm, x, y, alarm_x, alarm_y) VALUES({},{},{},{},{})".format(arm, x, y, 0 if alarmX == False else 1, 0 if alarmY == False else 1))
            self.db.commit()
        except:
            logging.error("Catch an exception.", exc_info=True)
    
    def initChart(self, arm, xy, xlim, ylim, bandInfo, parentWidget, clickMethod):
        # Chart X1
        vbl = QVBoxLayout(parentWidget)
        chart = Qt5MplCanvas(arm, xy, xlim, ylim, bandInfo, parentWidget)
        navigationToolbar = NavigationToolbar(chart, parentWidget)
        hbl = QHBoxLayout()
        hbl.addWidget(navigationToolbar)
        button = QPushButton("Configuration")
        button.setStyleSheet('background-color: rgb(85, 255, 127);')
        button.clicked.connect(clickMethod)
        hbl.addWidget(button)
        vbl.addLayout(hbl)
        vbl.addWidget(chart)

        return chart


    def initCamera(self):
        devices = pylon.TlFactory.GetInstance().EnumerateDevices()
        for device in devices:
            self.cbCamera1.addItem(device.GetSerialNumber())
            self.cbCamera2.addItem(device.GetSerialNumber())

        index1 = self.cbCamera1.findText(self.config['Arm1']['Camera'])
        self.cbCamera1.setCurrentIndex(index1)

        index2 = self.cbCamera2.findText(self.config['Arm2']['Camera'])
        self.cbCamera2.setCurrentIndex(index2)

        error = "OK"

        if index1 < 0:
            error = "Camera1 is not found."

        if index2 < 0:
            error = "Camera2 is not found."

        if index1 < 0 and index2 < 0:
            error = "Camera1 and Camera2 are not found."

        return (index1 >= 0 and index2 >= 0, error)

    def imageToWorld_1(self, u, v):
        uv_point = np.ones((3,1))
        uv_point[0] = u
        uv_point[1] = v 
        extrinsicMat=cv2.hconcat([self.rotation_matrix_1, self.translation_vector_1])
        projectionMatrix = np.dot(self.camera_matrix_1,  extrinsicMat)
        p11 = projectionMatrix[0, 0]
        p12 = projectionMatrix[0, 1]
        p14 = projectionMatrix[0, 3]
        p21 = projectionMatrix[1, 0]
        p22 = projectionMatrix[1, 1]
        p24 = projectionMatrix[1, 3]
        p31 = projectionMatrix[2, 0]
        p32 = projectionMatrix[2, 1]
        p34 = projectionMatrix[2, 3]
        homographyMatrix = np.array([p11, p12, p14, p21, p22, p24, p31, p32, p34]).reshape(3,3)
        inverseHomographyMatrix = np.linalg.inv(homographyMatrix)
        point3Dw = np.dot(inverseHomographyMatrix, uv_point)
        matPoint3D = point3Dw / point3Dw[2,0]
        return (matPoint3D[0][0], matPoint3D[1][0])


    def imageToWorld_2(self, u, v):
        uv_point = np.ones((3,1))
        uv_point[0] = u
        uv_point[1] = v 
        extrinsicMat=cv2.hconcat([self.rotation_matrix_2, self.translation_vector_2])
        projectionMatrix = np.dot(self.camera_matrix_2,  extrinsicMat)
        p11 = projectionMatrix[0, 0]
        p12 = projectionMatrix[0, 1]
        p14 = projectionMatrix[0, 3]
        p21 = projectionMatrix[1, 0]
        p22 = projectionMatrix[1, 1]
        p24 = projectionMatrix[1, 3]
        p31 = projectionMatrix[2, 0]
        p32 = projectionMatrix[2, 1]
        p34 = projectionMatrix[2, 3]
        homographyMatrix = np.array([p11, p12, p14, p21, p22, p24, p31, p32, p34]).reshape(3,3)
        inverseHomographyMatrix = np.linalg.inv(homographyMatrix)
        point3Dw = np.dot(inverseHomographyMatrix, uv_point)
        matPoint3D = point3Dw / point3Dw[2,0]
        return (matPoint3D[0][0], matPoint3D[1][0])

    def grabImage1(self, raw_data):
        QThreadPool.globalInstance().start(Runnable1(self, raw_data))
        


    def doUpdateImage1(self, raw_data):

        roi_x =  int(self.m_roi1.boundingRect().x() + 4 + self.m_roi1.pos().x())
        roi_y =  int(self.m_roi1.boundingRect().y() + 4 + self.m_roi1.pos().y())
        roi_w = int(self.m_roi1.boundingRect().width() - 8)
        roi_h = int(self.m_roi1.boundingRect().height() - 8)

        if self.cbDark1.isChecked():
            mask = np.ones(raw_data.shape, dtype=raw_data.dtype) * 255
            mask[roi_y:roi_y+roi_h, roi_x:roi_x+roi_w] = raw_data[roi_y:roi_y+roi_h, roi_x:roi_x+roi_w] 
            ret, thresh = cv2.threshold(mask, self.slThreshold1.value(), 255, cv2.THRESH_BINARY_INV)
        else:
            mask = np.zeros(raw_data.shape, dtype=raw_data.dtype)
            mask[roi_y:roi_y+roi_h, roi_x:roi_x+roi_w] = raw_data[roi_y:roi_y+roi_h, roi_x:roi_x+roi_w] 
            ret, thresh = cv2.threshold(mask, self.slThreshold1.value(), 255, cv2.THRESH_BINARY)

        gray = cv2.cvtColor(thresh, cv2.COLOR_RGB2GRAY)

        _, contours, hierarchy = cv2.findContours(gray, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        if contours:
            c = max(contours, key = cv2.contourArea)
            try:
                area = cv2.contourArea(c)
                if area > self.boxArea1.value():
                    # if area between a an b:
                    M = cv2.moments(c)
                    if M["m00"] > 0:
                        if self.ignoreCount1 > self.boxIgnore1.value():
                            fX = M["m10"] / M["m00"]
                            fY = M["m01"] / M["m00"]
                            pointWC = self.imageToWorld_1(fX, fY)
                            if self.world_x1 < pointWC[0] + 0.5:
                                self.world_x1 = pointWC[0]
                                cX = int(M["m10"] / M["m00"])
                                cY = int(M["m01"] / M["m00"])

                                #region update arm1 chart
                                (alarmX, alarmY) = self.checkAlarm(0, pointWC[0], pointWC[1])
                                self.insertTable(0, pointWC[0], pointWC[1], alarmX, alarmY)

                                if alarmX:
                                    self.insertAlarm(0, 0, pointWC[0], pointWC[1])

                                if alarmY:
                                    self.insertAlarm(0, 1, pointWC[0], pointWC[1])

                                # Coordinate X 
                                if len(self.arm1_x_chart_data) < self.arm1_x_xlim[1]:
                                    self.arm1_x_chart_data = np.concatenate(([pointWC[0]], self.arm1_x_chart_data))
                                else:
                                    self.arm1_x_chart_data = np.roll(self.arm1_x_chart_data, 1)
                                    self.arm1_x_chart_data[-1] = pointWC[0] 

                                # Coordinate Y
                                if len(self.arm1_y_chart_data) < self.arm1_y_xlim[1]:
                                    self.arm1_y_chart_data = np.concatenate(([pointWC[1]], self.arm1_y_chart_data))
                                else:
                                    self.arm1_y_chart_data = np.roll(self.arm1_y_chart_data, 1)
                                    self.arm1_y_chart_data[-1] = pointWC[1] 
                                #endregion

                                cv2.drawContours(raw_data, [c], -1, (0,255,0), 2)
                                cv2.circle(raw_data, (cX, cY), 7, (0,0,255), -1)
                                cv2.putText(raw_data, "{:.2f}, {:.2f}".format(pointWC[0], pointWC[1]), (cX-80, cY-20), cv2.FONT_HERSHEY_SIMPLEX, 1, (255,0,0), 2)
                                cv2.putText(raw_data, "{:.0f}".format(area), (cX-40, cY+40), cv2.FONT_HERSHEY_SIMPLEX, 1, (255,0,0), 2)
                            else:
                                self.ignoreCount1 = 0
                                self.world_x1 = 0
                        else:
                            self.ignoreCount1 = self.ignoreCount1 + 1
                            self.world_x1 = 0
                else:
                    self.ignoreCount1 = 0
                    self.world_x1 = 0
            except:
                logging.error("Catch an exception.", exc_info=True)

        inPoints = np.zeros((4, 3))
        inPoints[0]  = (-10 , 0 , 0)
        inPoints[1]  = (30 , 0 , 0)
        inPoints[2]  = (0, -10, 0)
        inPoints[3]  = (0, 30, 0)
        outPoints, jacobian = cv2.projectPoints(inPoints, self.rotation_vector_1, self.translation_vector_1, self.camera_matrix_1, self.dist_coeff_1)
        # Draw a diagonal blue line with thickness of 5 px
        cv2.arrowedLine(raw_data,(int(outPoints[0][0][0]),int(outPoints[0][0][1])),(int(outPoints[1][0][0]),int(outPoints[1][0][1])),(0,0,255),3,4)
        cv2.arrowedLine(raw_data,(int(outPoints[2][0][0]),int(outPoints[2][0][1])),(int(outPoints[3][0][0]),int(outPoints[3][0][1])),(255,0,0),3,4)

        img = QImage(raw_data.data, raw_data.shape[1], raw_data.shape[0], raw_data.shape[1] * 3, QImage.Format_RGB888) 
        self.m_pixmap1.setPixmap(QPixmap.fromImage(img))
        if self.cbFit1.isChecked():
            self.graphicsView1.fitInView(self.m_pixmap1, Qt.KeepAspectRatio)
        else:
            self.graphicsView1.resetTransform()

    def doUpdateImage2(self, raw_data):
        roi_x =  int(self.m_roi2.boundingRect().x() + 4 + self.m_roi2.pos().x())
        roi_y =  int(self.m_roi2.boundingRect().y() + 4 + self.m_roi2.pos().y())
        roi_w = int(self.m_roi2.boundingRect().width() - 8)
        roi_h = int(self.m_roi2.boundingRect().height() - 8)

        if self.cbDark2.isChecked():
            mask = np.ones(raw_data.shape, dtype=raw_data.dtype) * 255
            mask[roi_y:roi_y+roi_h, roi_x:roi_x+roi_w] = raw_data[roi_y:roi_y+roi_h, roi_x:roi_x+roi_w] 
            ret, thresh = cv2.threshold(mask, self.slThreshold2.value(), 255, cv2.THRESH_BINARY_INV)
        else:
            mask = np.zeros(raw_data.shape, dtype=raw_data.dtype)
            mask[roi_y:roi_y+roi_h, roi_x:roi_x+roi_w] = raw_data[roi_y:roi_y+roi_h, roi_x:roi_x+roi_w] 
            ret, thresh = cv2.threshold(mask, self.slThreshold2.value(), 255, cv2.THRESH_BINARY)
        gray = cv2.cvtColor(thresh, cv2.COLOR_RGB2GRAY)
        _, contours, hierarchy = cv2.findContours(gray, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        if contours:
            c = max(contours, key = cv2.contourArea)
            try:
                area = cv2.contourArea(c)
                if area > self.boxArea2.value():
                    # if area between a an b:
                    M = cv2.moments(c)
                    if M["m00"] > 0:
                        if self.ignoreCount2 > self.boxIgnore2.value():
                            fX = M["m10"] / M["m00"]
                            fY = M["m01"] / M["m00"]
                            pointWC = self.imageToWorld_2(fX, fY)
                            if self.world_x2 < pointWC[0] + 0.5:
                                self.world_x2 = pointWC[0]
                                cX = int(M["m10"] / M["m00"])
                                cY = int(M["m01"] / M["m00"])

                                #region update arm2 chart
                                (alarmX, alarmY) = self.checkAlarm(1, pointWC[0], pointWC[1])
                                self.insertTable(1, pointWC[0], pointWC[1], alarmX, alarmY)

                                if alarmX:
                                    self.insertAlarm(1, 0, pointWC[0], pointWC[1])

                                if alarmY:
                                    self.insertAlarm(1, 1, pointWC[0], pointWC[1])

                                # Coordinate X 
                                if len(self.arm2_x_chart_data) < self.arm2_x_xlim[1]:
                                    self.arm2_x_chart_data = np.concatenate(([pointWC[0]], self.arm2_x_chart_data))
                                else:
                                    self.arm2_x_chart_data = np.roll(self.arm2_x_chart_data, 1)
                                    self.arm2_x_chart_data[-1] = pointWC[0] 

                                # Coordinate Y
                                if len(self.arm2_y_chart_data) < self.arm2_y_xlim[1]:
                                    self.arm2_y_chart_data = np.concatenate(([pointWC[1]], self.arm2_y_chart_data))
                                else:
                                    self.arm2_y_chart_data = np.roll(self.arm2_y_chart_data, 1)
                                    self.arm2_y_chart_data[-1] = pointWC[1] 
                                #endregion

                                cv2.drawContours(raw_data, [c], -1, (0,255,0), 2)
                                cv2.circle(raw_data, (cX, cY), 7, (0,0,255), -1)
                                cv2.putText(raw_data, "{:.2f}, {:.2f}".format(pointWC[0], pointWC[1]), (cX-80, cY-20), cv2.FONT_HERSHEY_SIMPLEX, 1, (255,0,0), 2)
                                cv2.putText(raw_data, "{:.0f}".format(area), (cX-80, cY+20), cv2.FONT_HERSHEY_SIMPLEX, 1, (255,0,0), 2)
                            else:
                                self.ignoreCount2 = 0
                                self.world_x2 = 0

                        else:
                            self.ignoreCount2 = self.ignoreCount2 + 1
                            self.world_x2 = 0
                else:
                    self.ignoreCount2 = 0
                    self.world_x2 = 0
            except Exception as ex:
                logging.error("Catch an exception.", exc_info=True)

        inPoints = np.zeros((4, 3))
        inPoints[0]  = (-10 , 0 , 0)
        inPoints[1]  = (30 , 0 , 0)
        inPoints[2]  = (0, -10, 0)
        inPoints[3]  = (0, 30, 0)
        outPoints, jacobian = cv2.projectPoints(inPoints, self.rotation_vector_2, self.translation_vector_2, self.camera_matrix_2, self.dist_coeff_2)
        # Draw a diagonal blue line with thickness of 5 px
        cv2.arrowedLine(raw_data,(int(outPoints[0][0][0]),int(outPoints[0][0][1])),(int(outPoints[1][0][0]),int(outPoints[1][0][1])),(0,0,255),3,4)
        cv2.arrowedLine(raw_data,(int(outPoints[2][0][0]),int(outPoints[2][0][1])),(int(outPoints[3][0][0]),int(outPoints[3][0][1])),(255,0,0),3,4)

        img = QImage(raw_data.data, raw_data.shape[1], raw_data.shape[0], raw_data.shape[1] * 3, QImage.Format_RGB888) 
        self.m_pixmap2.setPixmap(QPixmap.fromImage(img))
        if self.cbFit2.isChecked():
            self.graphicsView2.fitInView(self.m_pixmap2, Qt.KeepAspectRatio)
        else:
            self.graphicsView2.resetTransform()

    def open_close_camera1(self):
        if self.btnOpen1.text() == "Open":
            sn = self.cbCamera1.currentText()
            devices = pylon.TlFactory.GetInstance().EnumerateDevices()
            for device in devices:
                if device.GetSerialNumber() == sn:
                    self.camera1 = pylon.InstantCamera(pylon.TlFactory.GetInstance().CreateDevice(device))
                    self.btnOpen1.setStyleSheet("background-color: rgb(85, 255, 127);")
                    self.btnOpen1.setText("Close")
                    self.btnStart1.setStyleSheet("background-color: rgb(255, 170, 255);")
                    self.btnStart1.setEnabled(True)
                    (self.camera_matrix_1, self.dist_coeff_1, self.rotation_vector_1, self.translation_vector_1, self.rotation_matrix_1) = self.loadCalibration(sn)
                    break
        else:
            self.camera1 = None
            self.btnOpen1.setStyleSheet("background-color: rgb(255, 170, 255);")
            self.btnOpen1.setText("Open")
            self.btnStart1.setStyleSheet("")
            self.btnStart1.setEnabled(False)

    def start_stop_camera1(self):
        try:
            if self.btnStart1.text() == "Start":
                if self.camera1:
                    self.sample1 = SampleImageEventHandler()
                    self.sample1.updateImage.connect(self.doUpdateImage1)
                    self.camera1.RegisterConfiguration(pylon.AcquireContinuousConfiguration(), pylon.RegistrationMode_ReplaceAll, pylon.Cleanup_Delete)
                    self.camera1.RegisterImageEventHandler(self.sample1, pylon.RegistrationMode_ReplaceAll, pylon.Cleanup_Delete)
                    self.camera1.StartGrabbing(pylon.GrabStrategy_OneByOne, pylon.GrabLoop_ProvidedByInstantCamera)
                    # pylon.FeaturePersistence.Save('camera1.txt', self.camera1.GetNodeMap())
                    self.camera1.ExposureAuto.SetValue('Off')
                    self.camera1.ExposureTimeRaw.SetValue(int(self.config['Arm1']['ExposureTimeRaw']))
                    self.slExposure1.setValue(self.camera1.ExposureTimeRaw.GetValue())
                    self.lbExposure1.setText("Exposure: {}".format(self.camera1.ExposureTimeRaw.GetValue()))
                    self.camera1.GainRaw.SetValue(int(self.config['Arm1']['GainRaw']))
                    self.slGain1.setValue(self.camera1.GainRaw.GetValue())
                    self.lbGain1.setText("Gain: {}".format(self.camera1.GainRaw.GetValue()))
                    self.btnStart1.setStyleSheet("background-color: rgb(85, 255, 127);")
                    self.btnStart1.setText("Stop")
                    self.slExposure1.setEnabled(True)
                    self.slGain1.setEnabled(True)
                    self.slThreshold1.setEnabled(True)
                    self.cbDark1.setEnabled(True)
                    self.cbFit1.setEnabled(True)
                    self.boxIgnore1.setEnabled(True)
                    self.boxArea1.setEnabled(True)
                    self.btnOpen1.setEnabled(False)
                    self.timerArm1 = RepeatingTimer(3, self.update_arm1_chart)
                    self.timerArm1.start()
            else:
                if self.camera1:
                    self.camera1.StopGrabbing()
                    self.camera1.Close()
                    self.btnStart1.setStyleSheet("background-color: rgb(255, 170, 255);")
                    self.btnStart1.setText("Start")
                    self.slExposure1.setEnabled(False)
                    self.slGain1.setEnabled(False)
                    self.slThreshold1.setEnabled(False)
                    self.cbDark1.setEnabled(False)
                    self.cbFit1.setEnabled(False)
                    self.boxIgnore1.setEnabled(False)
                    self.boxArea1.setEnabled(False)
                    self.btnOpen1.setEnabled(True)
                    self.timerArm1.cancel()
        except Exception as ex:
            msg = QMessageBox()
            msg.setWindowIcon(QIcon('arm.ico'))
            msg.setIcon(QMessageBox.Critical)
            msg.setWindowTitle('Robot Arm Coordinate Monitor')
            msg.setText(str(ex))
            msg.setStandardButtons(QMessageBox.Ok)
            msg.exec_()


    def open_close_camera2(self):
        if self.btnOpen2.text() == "Open":
            sn = self.cbCamera2.currentText()
            devices = pylon.TlFactory.GetInstance().EnumerateDevices()
            for device in devices:
                if device.GetSerialNumber() == sn:
                    self.camera2 = pylon.InstantCamera(pylon.TlFactory.GetInstance().CreateDevice(device))
                    self.btnOpen2.setStyleSheet("background-color: rgb(85, 255, 127);")
                    self.btnOpen2.setText("Close")
                    self.btnStart2.setStyleSheet("background-color: rgb(255, 170, 255);")
                    self.btnStart2.setEnabled(True)
                    (self.camera_matrix_2, self.dist_coeff_2, self.rotation_vector_2, self.translation_vector_2, self.rotation_matrix_2) = self.loadCalibration(sn)
                    break
        else:
            self.camera2 = None
            self.btnOpen2.setStyleSheet("background-color: rgb(255, 170, 255);")
            self.btnOpen2.setText("Open")
            self.btnStart2.setStyleSheet("")
            self.btnStart2.setEnabled(False)

    def start_stop_camera2(self):
        try:
            if self.btnStart2.text() == "Start":
                if self.camera2:
                    self.sample2 = SampleImageEventHandler()
                    self.sample2.updateImage.connect(self.doUpdateImage2)
                    self.camera2.RegisterConfiguration(pylon.AcquireContinuousConfiguration(), pylon.RegistrationMode_ReplaceAll, pylon.Cleanup_Delete)
                    self.camera2.RegisterImageEventHandler(self.sample2, pylon.RegistrationMode_ReplaceAll, pylon.Cleanup_Delete)
                    self.camera2.StartGrabbing(pylon.GrabStrategy_OneByOne, pylon.GrabLoop_ProvidedByInstantCamera)
                    # pylon.FeaturePersistence.Save('camera2.txt', self.camera2.GetNodeMap())
                    self.camera2.ExposureAuto.SetValue('Off')
                    self.camera2.ExposureTimeRaw.SetValue(int(self.config['Arm2']['ExposureTimeRaw']))
                    self.slExposure2.setValue(self.camera2.ExposureTimeRaw.GetValue())
                    self.lbExposure2.setText("Exposure: {}".format(self.camera2.ExposureTimeRaw.GetValue()))
                    self.camera2.GainRaw.SetValue(int(self.config['Arm2']['GainRaw']))
                    self.slGain2.setValue(self.camera2.GainRaw.GetValue())
                    self.lbGain2.setText("Gain: {}".format(self.camera2.GainRaw.GetValue()))
                    self.btnStart2.setStyleSheet("background-color: rgb(85, 255, 127);")
                    self.btnStart2.setText("Stop")
                    self.slExposure2.setEnabled(True)
                    self.slGain2.setEnabled(True)
                    self.slThreshold2.setEnabled(True)
                    self.cbDark2.setEnabled(True)
                    self.cbFit2.setEnabled(True)
                    self.boxIgnore2.setEnabled(True)
                    self.boxArea2.setEnabled(True)
                    self.btnOpen2.setEnabled(False)
                    self.timerArm2 = RepeatingTimer(3, self.update_arm2_chart)
                    self.timerArm2.start()
            else:
                if self.camera2:
                    self.camera2.StopGrabbing()
                    self.camera2.Close()
                    self.btnStart2.setStyleSheet("background-color: rgb(255, 170, 255);")
                    self.btnStart2.setText("Start")
                    self.slExposure2.setEnabled(False)
                    self.slGain2.setEnabled(False)
                    self.slThreshold2.setEnabled(False)
                    self.cbDark2.setEnabled(False)
                    self.cbFit2.setEnabled(False)
                    self.boxIgnore2.setEnabled(False)
                    self.boxArea2.setEnabled(False)
                    self.btnOpen2.setEnabled(True)
                    self.timerArm2.cancel()
        except Exception as ex:
            msg = QMessageBox()
            msg.setWindowIcon(QIcon('arm.ico'))
            msg.setIcon(QMessageBox.Critical)
            msg.setWindowTitle('Robot Arm Coordinate Monitor')
            msg.setText(str(ex))
            msg.setStandardButtons(QMessageBox.Ok)
            msg.exec_()

    
    def change_exposure1(self, value):
        self.lbExposure1.setText("Exposure: {}".format(value))
        if value != self.camera1.ExposureTimeRaw.GetValue():
            self.camera1.ExposureTimeRaw.SetValue(value)
            self.config['Arm1']['exposuretimeraw'] = str(value)

    def change_gain1(self, value):
        self.lbGain1.setText("Gain: {}".format(value))
        if value != self.camera1.GainRaw.GetValue():
            self.camera1.GainRaw.SetValue(value)
            self.config['Arm1']['gainraw'] = str(value)

    def change_exposure2(self, value):
        self.lbExposure2.setText("Exposure: {}".format(value))
        if value != self.camera2.ExposureTimeRaw.GetValue():
            self.camera2.ExposureTimeRaw.SetValue(value)
            self.config['Arm2']['exposuretimeraw'] = str(value)

    def change_gain2(self, value):
        self.lbGain2.setText("Gain: {}".format(value))
        if value != self.camera2.GainRaw.GetValue():
            self.camera2.GainRaw.SetValue(value)
            self.config['Arm2']['gainraw'] = str(value)

    def change_threshold1(self, value):
        self.lbThreshold1.setText("Threshold: {}".format(value))

    def change_dark1(self, value):
        self.dark1 = value == 2


    def change_threshold2(self, value):
        self.lbThreshold2.setText("Threshold: {}".format(value))

    def change_dark2(self, value):
        self.dark2 = value == 2

    #region Arm1 Chart
    def update_arm1_chart(self):
        self.update_arm1_x_chart()
        self.update_arm1_y_chart()

    def update_arm1_x_chart(self):
        data = self.arm1_x_chart_data
        self.arm1_x_chart.update_data(data, self.bandInfo)

    def update_arm1_y_chart(self):
        data = self.arm1_y_chart_data
        self.arm1_y_chart.update_data(data, self.bandInfo)

    def arm1_x_config(self):
        try:
            dialog = BandDialog(0, 0, self.db, self.arm1_x_xlim, self.arm1_x_ylim, self.bandInfo)
            dialog.exec_()
            self.queryBandInfo()
            self.update_arm1_x_chart()
        except:
            logging.error("Catch an exception.", exc_info=True)

    def arm1_y_config(self):
        try:
            dialog = BandDialog(0, 1, self.db, self.arm1_y_xlim, self.arm1_y_ylim, self.bandInfo)
            dialog.exec_()
            self.queryBandInfo()
            self.update_arm1_y_chart()
        except:
            logging.error("Catch an exception.", exc_info=True)
    #endregion

    #region Arm2 Chart
    def update_arm2_chart(self):
        self.update_arm2_x_chart()
        self.update_arm2_y_chart()

    def update_arm2_x_chart(self):
        data = self.arm2_x_chart_data
        self.arm2_x_chart.update_data(data, self.bandInfo)

    def update_arm2_y_chart(self):
        data = self.arm2_y_chart_data
        self.arm2_y_chart.update_data(data, self.bandInfo)

    def arm2_x_config(self):
        try:
            dialog = BandDialog(1, 0, self.db, self.arm2_x_xlim, self.arm2_x_ylim, self.bandInfo)
            dialog.exec_()
            self.queryBandInfo()
            self.update_arm1_x_chart()
        except:
            logging.error("Catch an exception.", exc_info=True)

    def arm2_y_config(self):
        try:
            dialog = BandDialog(1, 1, self.db, self.arm2_y_xlim, self.arm2_y_ylim, self.bandInfo)
            dialog.exec_()
            self.queryBandInfo()
            self.update_arm1_y_chart()
        except:
            logging.error("Catch an exception.", exc_info=True)
    #endregion
    def show_calibration(self):
        try:
            dialog = CalibrationDialog(self.config)
            dialog.exec_()
            (self.camera_matrix_1, self.dist_coeff_1, self.rotation_vector_1, self.translation_vector_1, self.rotation_matrix_1) = self.loadCalibration(self.cbCamera1.currentText())
            (self.camera_matrix_2, self.dist_coeff_2, self.rotation_vector_2, self.translation_vector_2, self.rotation_matrix_2) = self.loadCalibration(self.cbCamera2.currentText())
        except:
            logging.error("Catch an exception.", exc_info=True)


if __name__ == '__main__':
    application = QApplication(sys.argv)

    log_dir = "{}/log".format(os.path.dirname(os.path.abspath(__file__)))
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)
    log_file = "{}/log.txt".format(log_dir)

    formatter = logging.Formatter('%(asctime)s - %(filename)s:%(lineno)d - %(levelname)s - %(message)s')
    formatter.converter = time.gmtime

    fileHandler = logging.handlers.RotatingFileHandler(log_file, maxBytes=1024 * 1024 * 5, backupCount=10)
    fileHandler.setFormatter(formatter)

    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(formatter)

    logger = logging.getLogger()
    logger.addHandler(fileHandler)
    logger.addHandler(consoleHandler)
    

    if len(sys.argv) > 1:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.ERROR)


    license_file = 'license.dat'
    has_dongle = ETEnum() > 0
    if has_dongle and os.path.isfile(license_file):
        splash = QSplashScreen(QPixmap('logo.jpg'))
        splash.show()

        context = ET_CONTEXT()
        dongle_sn = ETOpen(context)
        ETClose(context)
        
        fileManager = FileManager("望渠消息向長安 常把菱花仔細看")
        fileManager.DecryptFile(license_file)
        if fileManager.Address == dongle_sn:
            if os.path.isfile('PyArm.ini'):
                main_window = MainWindow()
                db_ok = main_window.initDatabase()
                camera_ok = main_window.initCamera()
                if db_ok[0] and camera_ok[0]:
                    splash.close()
                    main_window.showMaximized()
                    sys.exit(application.exec_())
                else:
                    splash.close()
                    msg = QMessageBox()
                    msg.setWindowIcon(QIcon('arm.ico'))
                    msg.setIcon(QMessageBox.Critical)
                    msg.setWindowTitle('Robot Arm Coordinate Monitor')
                    if db_ok[0] == False and camera_ok[0] == False:
                        msg.setText("{} and {} The Application will shutdown.".format(db_ok[1], camera_ok[1]))
                    elif db_ok[0] == False:
                        msg.setText("{} The Application will shutdown.".format(db_ok[1]))
                    else:
                        msg.setText("{} The Application will shutdown.".format(camera_ok[1]))
                    logging.error(msg.text())
                    msg.setStandardButtons(QMessageBox.Ok)
                    msg.exec_()
            else:
                logging.error("The parameter files are not found. The Application will shutdown.")
                splash.close()
                msg = QMessageBox()
                msg.setWindowIcon(QIcon('arm.ico'))
                msg.setIcon(QMessageBox.Critical)
                msg.setWindowTitle('Robot Arm Coordinate Monitor')
                msg.setText("The parameter files are not found. The Application will shutdown.")
                msg.setStandardButtons(QMessageBox.Ok)
                msg.exec_()
        else:
            logging.error("The dongle is not match with licene file. The Application will shutdown.")
            splash.close()
            msg = QMessageBox()
            msg.setWindowIcon(QIcon('arm.ico'))
            msg.setIcon(QMessageBox.Critical)
            msg.setWindowTitle('Robot Arm Coordinate Monitor')
            msg.setText("The dongle is not match with licene file. The Application will shutdown.")
            msg.setStandardButtons(QMessageBox.Ok)
            msg.exec_()

    else:
        logging.error("Don't find license file or dongle. The Application will shutdown.")
        msg = QMessageBox()
        msg.setWindowIcon(QIcon('arm.ico'))
        msg.setIcon(QMessageBox.Critical)
        msg.setWindowTitle('Robot Arm Coordinate Monitor')
        msg.setText("Don't find license file or dongle. The Application will shutdown.")
        msg.setStandardButtons(QMessageBox.Ok)
        msg.exec_()